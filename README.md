# Worlds Without Number for Foundry VTT (Unofficial)
All-- Most-- SOME the features you need to play Worlds Without Number in Foundry VTT. This was forked from v1.1.2 of the Old School Essentials project developed by U~man. I have managed to mangle his beautiful project into something that sort of works for Worlds Without Number. All praise should be directed toward him. Any bugs or mistakes are undoubtedly my own.

Find the original OSE project by U~man here: https://gitlab.com/mesfoliesludiques/foundryvtt-ose
## Features
* Calculated Readied/Stowed values
* Easy tracking of weapon tags
* Track which Arts have Effort committed
* Calculated Effort totals from up to three sources
    * Click Tweaks in the character title bar to activate spellcasting and enter caster class(es)
* Visual indicator of health/strain percentage
* Auto-populate monster saves
* Automatically calculates movement rates based on Readied/Stowed values
    * This can be disabled in Tweaks. Currently this only enables manual entering of Exploration Movement Rate, from which the others will still be calculated. This will be changed later.
* Adds Attribute/Skill bonuses to hit rolls and Attribute bonus to damage rolls
## TODO
* Sorting Arts/Spells by source (class)
* Toggle to add Skill value to damage
* Manual entry of all movement modes
* Auto-add highest Dex mod when using Group Initiative
## Installation
To install, copy the following URL and paste it into the Install System dialog in Foundry VTT:
https://gitlab.com/sobran/foundryvtt-wwn/-/raw/master/system.json

It will be available through Foundry itself at a future date.

## License
This Foundry VTT system requires the Worlds Wihtout Number rules, available through the Kickstarter or through sale at some point in the future.

This third party product is not affiliated with or approved by Sine Nomine Publishing. \
Worlds Without Number is a trademark of Sine Nomine Publishing. \

## Artwork
Weapon quality icons, and the Treasure chest (and now some others) are from [Rexxard](https://assetstore.unity.com/packages/2d/gui/icons/flat-skills-icons-82713).